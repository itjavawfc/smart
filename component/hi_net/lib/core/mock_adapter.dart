//测试适配器，mock 数据

import '../request/hi_base_request.dart';
import 'hi_net_adapter.dart';

class MockAdapter extends HiNetAdapter {
  @override
  Future<HiNetResponse> send<T>(HiBaseRequest request) {
    return Future<HiNetResponse>.delayed(Duration(milliseconds: 1000), () {
      return HiNetResponse(
          data: {'code': 0, 'message': 'success!'}, statusCode: 401);
    });
  }
}
