import 'package:hi_net/request/hi_base_request.dart';

import 'base_request.dart';

class RankingRequest extends BaseRequest {
  //https://api.devio.org/uapi/fa/ranking?sort=like&pageIndex=1&pageSize=40
  @override
  HttpMethod httpMethod() {
    return HttpMethod.GET;
  }

  @override
  bool needLogin() {
    return true;
  }

  @override
  String path() {
    return 'uapi/fa/ranking';
  }
}
