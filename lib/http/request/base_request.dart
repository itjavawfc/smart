import 'package:hi_net/request/hi_base_request.dart';

import '../../util/hi_constants.dart';
import '../dao/login_dao.dart';

abstract class BaseRequest extends HiBaseRequest {
  @override
  String url() {
    if (needLogin()) {
      //给需要登录的接口携带登录令牌
      print(
          ' Base Request  needLogin 保存的 BOARDING_PASS ${LoginDao.getBoardingPass()}');
      addHeader(LoginDao.BOARDING_PASS, LoginDao.getBoardingPass());
    }
    return super.url();
  }

  Map<String, dynamic> header = {
    HiConstants.authTokenK: HiConstants.authTokenV,
    //访问令牌，在课程公告中获取
    HiConstants.courseFlagK: HiConstants.courseFlagV,
  };
}
