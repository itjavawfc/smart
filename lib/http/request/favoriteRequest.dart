import 'package:hi_net/request/hi_base_request.dart';

import 'base_request.dart';

// https://api.devio.org/uapi/fa/favorite/BV1nA411E7tm
//curl -X POST "https://api.devio.org/uapi/fa/favorite/BV1nA411E7tm" -H "accept: */*" -H "course-flag: fa"

class FavoriteRequest extends BaseRequest {
  @override
  HttpMethod httpMethod() {
    return HttpMethod.POST;
  }

  @override
  bool needLogin() {
    return true;
  }

  @override
  String path() {
    return 'uapi/fa/favorite';
  }
}
