import 'package:hi_net/hi_net.dart';
import 'package:smart/http/request/HomeRequest.dart';
import 'package:smart/model/home_mo.dart';

class HomeDao {
  //https://api.devio.org/uapi/fa/home/推荐?pageIndex=1&pageSize=10
  // 正确的访问地址：https://api.devio.org/uapi/fa/home/%E6%8E%A8%E8%8D%90?pageIndex=1&pageSize=10"
  // https://api.devio.org/uapi/fa/home/%E6%8E%A8%E8%8D%90?pageIndex=1&pageSize=1&boarding-pass=78C630C355384D710714EB539F06E44EAF
  //curl -X GET "https://api.devio.org/uapi/fa/home/%E6%8E%A8%E8%8D%90?pageIndex=1&pageSize=10" -H "accept: */*" -H "course-flag: fa"
  static get(String categoryName, {int pageIndex = 1, int pageSize = 1}) async {
    HomeRequest request = HomeRequest();
    request.pathParams = categoryName;
    //request.addHeader('course-flag', 'fa');
    request.add('pageIndex', pageIndex).add('pageSize', pageSize);
    //  request.add('pageIndex', pageIndex).add('pageSize', 10);  //零时pageSize 写死：10 ,友好展示列表数据。
    // .add('course-flag', 'fa'); //course-flag
    var result = await HiNet.getInstance().fire(request);
    //  print('Home Dao result:$result');
    return HomeMo.fromJson(result['data']);
  }
}
