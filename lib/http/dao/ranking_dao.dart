import 'package:hi_net/hi_net.dart';
import 'package:smart/model/ranking_mo.dart';

import '../request/ranking_request.dart';

class RankingDao {
  //curl -X GET "https://api.devio.org/uapi/fa/ranking?sort=like&pageIndex=1&pageSize=40" -H "accept: */*" -H "course-flag: fa"
  /*
  course-flag 课程标识
  sort        排行规则，接受：like：最热、pubdate：最新、favorite：收藏
  pageIndex   页码
  pageSize    每页显示条数


   */
  static get(String sort, {int pageIndex = 1, pageSize = 10}) async {
    RankingRequest request = RankingRequest();
    request
        .add('sort', sort)
        .add('pageIndex', pageIndex)
        .add('pageSize', pageSize);
    var result = await HiNet.getInstance().fire(request);
    print('RankingDao result:$result');
    return RankingMo.fromJson(result['data']);
  }
}
