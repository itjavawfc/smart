import 'package:hi_net/hi_net.dart';
import 'package:smart/http/request/FavoriteRequest.dart';
import 'package:smart/http/request/cancel_favoriteRequest.dart';

import '../../model/ranking_mo.dart';
import '../request/base_request.dart';
import '../request/favorite_list_request.dart';

class FavoriteDao {
  // https://api.devio.org/uapi/fa/favorite/BV1nA411E7tm
  //curl -X POST "https://api.devio.org/uapi/fa/favorite/BV1nA411E7tm" -H "accept: */*" -H "course-flag: fa"
  static favorite(String vid, bool favorite) async {
    BaseRequest request =
        favorite ? FavoriteRequest() : CancelFavoriteRequest();
    request.pathParams = vid;
    request.add('vid', vid);

    var result = await HiNet.getInstance().fire(request);
    print(result);
    return result;
  }

  //https://api.devio.org/uapi/fa/favorites?pageIndex=1&pageSize=10
  static favoriteList({int pageIndex = 1, int pageSize = 10}) async {
    FavoriteListRequest request = FavoriteListRequest();
    request.add("pageIndex", pageIndex).add("pageSize", pageSize);
    var result = await HiNet.getInstance().fire(request);
    print(result);
    return RankingMo.fromJson(result['data']);
  }
}
