import 'package:hi_net/hi_net.dart';
import 'package:smart/db/hi_cache.dart';
import 'package:smart/http/request/RegistrationRequest.dart';
import 'package:smart/http/request/login_request.dart';

import '../request/base_request.dart';

class LoginDao {
  static const BOARDING_PASS = 'boarding-pass';

  static login(String userName, String password) {
    return _send(userName, password);
  }

  static registration(
      String userName, String password, String immocId, String orderId) {
    return _send(userName, password, immocId: immocId, orderId: orderId);
  }

  // 验证码是可选参数，如果存在验证码那就是注册，不存在验证码就是登录
  static _send(String userName, String password, {immocId, orderId}) async {
    BaseRequest request;
    if (immocId != null && orderId != null) {
      print('send->RegistrationRequest');
      request = RegistrationRequest();
    } else {
      print('send->LoginRequest');
      request = LoginRequest();
    }
    request
        .add('userName', userName)
        .add('password', password)
        .add('imoocId', immocId ?? '')
        .add('orderId', orderId ?? ''); //2311242343014816
    var result = await HiNet.getInstance().fire(request);
    print(result);
    if (result['code'] == 0 && result['data'] != null) {
      //保存登录令牌
      print('  regist or logn  save BOARDING_PASS:' + result['data']);
      HiCache.getInstance()!.setString(BOARDING_PASS, result['data']);
    }
    return result;
  }

  static getBoardingPass() {
    return HiCache.getInstance()!.get(BOARDING_PASS);
  }
}
