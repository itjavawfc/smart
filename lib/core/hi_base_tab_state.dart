import 'package:flutter/material.dart';
import 'package:hi_base/color.dart';
import 'package:hi_base/hi_state.dart';
import 'package:hi_net/core/hi_error.dart';

import '../util/toast.dart';

abstract class HiBaseTabState<M, L, T extends StatefulWidget> extends HiState<T>
    with AutomaticKeepAliveClientMixin {
  List<L> dataList = [];
  int pageIndex = 1;
  bool loading = false;
  ScrollController scrollController = ScrollController();
  get contentChild;
  @override
  void initState() {
    super.initState();
    scrollController.addListener(() {
      var dis = scrollController.position.maxScrollExtent -
          scrollController.position.pixels;
      print('dis:$dis');
      //当距离底部不足300时加载更多
      if (dis < 300 &&
          !loading &&
          //fix 当列表高度不满屏幕高度时不执行加载更多
          scrollController.position.maxScrollExtent != 0) {
        //scrollController.position.maxScrollExtent != 0) {
        print('------_loadData---');
        loadData(loadMore: true);
      }
    });
    loadData();
  }

  @override
  void dispose() {
    super.dispose();
    scrollController.dispose(); //controller 相关的  在界面 dispose时候，需要主动销毁掉
  }

  /* @override
  Widget build(BuildContext context) {
    super.build(context);
    //return contentChild;
    return RefreshIndicator(
      onRefresh: loadData,
      color: grey,
      child: MediaQuery.removePadding(
          removeTop: true, context: context, child: contentChild),
    );
  }*/

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return RefreshIndicator(
      onRefresh: loadData,
      color: primary,
      child: MediaQuery.removePadding(
          removeTop: true, context: context, child: contentChild),
    );
  }

  //获取对应页码的数据
  Future<M> getData(int pageIndex);
  //从MO中解析出list数据
  List<L> parseList(M result);

  Future<void> loadData({loadMore = false}) async {
    if (loading) {
      print('上次加载还未完成 ...');
      return;
    }

    loading = true;
    if (!loadMore) {
      pageIndex = 1;
    }

    print('加载数据 loadData ');
    var currentIndex = pageIndex + (loadMore ? 1 : 0);

    try {
      var result = await getData(currentIndex);
      setState(() {
        //刷新界面
        if (loadMore) {
          // videoList = [...videoList, ...result.videoList!];
          //合成一个新数组
          dataList = [...dataList, ...parseList(result)];

          if (parseList(result).length != 0) {
            //做一层保护
            pageIndex++;
          }
        } else {
          dataList = parseList(result);
        }
        /*   List<VideoModel> tempList=[...videoList,...videoList];  //扩充数据，便于测试验证
        videoList=[...tempList,...tempList];*/
        print('_loadData  刷新后 当前数据集合大小:${dataList.length}');
      });

      Future.delayed(Duration(milliseconds: 1000), () {
        print('从数据加载到数据 到渲染 是需要时间，延时一秒钟后重置加载更多 为 ');
        loading = false;
      });
    } on NeedAuth catch (e) {
      loading = false; //加载出错的时候，重新设置为false
      print('HomeTabPage   NeedAuth ');
      print(e);
      showWarnToast(e.message);
    } on HiNetError catch (e) {
      loading = false;
      print('HomeTabPage   HiNetError ');
      print(e);
      showWarnToast(e.message);
    }
  }

  @override
  bool get wantKeepAlive => true;
}
