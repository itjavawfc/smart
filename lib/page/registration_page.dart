import 'package:flutter/material.dart';
import 'package:hi_base/string_util.dart';
import 'package:hi_net/core/hi_error.dart';
import 'package:smart/navigator/hi_navigator.dart';
import 'package:smart/widget/login_effect.dart';
import 'package:smart/widget/login_input.dart';

import '../http/dao/login_dao.dart';
import '../util/toast.dart';
import '../widget/appbar.dart';

class RegistrationPage extends StatefulWidget {
  // final VoidCallback? onJumpToLogin;

  const RegistrationPage({Key? key}) : super(key: key);

  @override
  State<RegistrationPage> createState() => _RegistrationPageState();
}

class _RegistrationPageState extends State<RegistrationPage> {
  bool protect = false;
  bool loginEnable = false; // 是否可登录
  String? userName;
  String? password;
  String? rePassword;
  String? immocId;
  String? orderId;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.white),
        useMaterial3: true,
      ),
      home: Scaffold(
        appBar: appBar('注册', '登录',
            () => {HiNavigator.getInstance().onJumpTo(RouteStatus.login)}),
        //  appBar: appBar('注册', '登录', () {}),
        body: Container(
          // height: 200,
          child: ListView(children: [
            LoginEffect(protect: protect),
            LoginInput(
              '用户名',
              '请输入用户名',
              onChanged: (text) {
                //print(text);
                userName = text;
                checkInput();
              },
            ),
            LoginInput(
              '密码',
              '请输入密码',
              obscureText: true,
              lineStretch: true,
              onChanged: (text) {
                //print(text);
                password = text;
                checkInput();
              },
              focusChanged: (focus) {
                this.setState(() {
                  print('focus:$focus');
                  protect = focus;
                });
              },
            ),
            LoginInput(
              '确认密码',
              '请再次输入密码',
              obscureText: true,
              lineStretch: true,
              onChanged: (text) {
                //print(text);
                rePassword = text;
                checkInput();
              },
              focusChanged: (focus) {
                this.setState(() {
                  print('focus:$focus');
                  protect = focus;
                });
              },
            ),
            LoginInput(
              'ID号',
              '请输入您的ID号',
              obscureText: true,
              lineStretch: true,
              keyboardType: TextInputType.number,
              onChanged: (text) {
                //print(text);
                immocId = text;
                checkInput();
              },
              focusChanged: (focus) {
                this.setState(() {
                  print('focus:$focus');
                  protect = focus;
                });
              },
            ),
            LoginInput(
              '课程订单号',
              '请输入课程订单号后四位',
              obscureText: true,
              lineStretch: true,
              keyboardType: TextInputType.number,
              onChanged: (text) {
                //print(text);
                orderId = text;
                checkInput();
              },
              focusChanged: (focus) {
                this.setState(() {
                  print('focus:$focus');
                  protect = focus;
                });
              },
            ),
            Padding(
                padding: EdgeInsets.only(top: 20, left: 20, right: 20),
                child: _loginButton())
          ]),
        ),
      ),
    );
  }

  /*
   bool protect = false;
  bool loginEnable = false; // 是否可登录
  String? userName;
  String? password;
  String? rePassword;
  String? immocId;
  String? orderId;
   */
  void checkInput() {
    bool enable;
    if (isNotEmpty(userName!) &&
        isNotEmpty(password!) &&
        isNotEmpty(rePassword!) &&
        isNotEmpty(immocId!) &&
        isNotEmpty(orderId!)) {
      enable = true;
    } else {
      enable = false;
    }
    setState(() {
      loginEnable = enable;
    });
  }

  _loginButton() {
    return InkWell(
      onTap: () {
        if (loginEnable) {
          checkParams();
        } else {
          print('loginEnable is false');
        }
      },
      child: Text('注册'),
    );
  }

  void send() async {
    print('发送网络请求，进行注册...');

    try {
      var result =
          await LoginDao.registration(userName!, password!, immocId!, orderId!);
      print(result);
      if (result['code'] == 0) {
        print('注册成功');
        showToast('注册成功');
        /*if (widget.onJumpToLogin != null) {
          widget.onJumpToLogin!();
        }*/
        print('注册成功后，跳转到登录页面');
        HiNavigator.getInstance().onJumpTo(RouteStatus.login);
      } else {
        print(result['msg']);
        showWarnToast(result['msg']);
      }
    } on NeedAuth catch (e) {
      print(e);
      showWarnToast(e.message);
    } on HiNetError catch (e) {
      print(e);
      showWarnToast(e.message);
    }
  }

  void checkParams() {
    String? tips;
    if (password != rePassword) {
      tips = '两次密码不一致!';
    } else if (orderId!.length != 4) {
      tips = '请输入订单后四位!';
    }
    if (tips != null) {
      print(tips);
      return;
    }
    send();
  }
}
