import 'package:flutter/material.dart';
import 'package:hi_base/view_util.dart';
import 'package:hi_net/core/hi_error.dart';
import 'package:smart/http/dao/profile_dao.dart';
import 'package:smart/model/profile_mo.dart';
import 'package:smart/widget/course_card.dart';
import 'package:smart/widget/hi_banner.dart';
import 'package:smart/widget/hi_blur.dart';
import 'package:smart/widget/hi_flexble_header.dart';

import '../util/toast.dart';
import '../widget/banefit_card.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({super.key});

  @override
  State<ProfilePage> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage>
    with AutomaticKeepAliveClientMixin {
  ProfileMo? _profileMo;
  ScrollController _controller = ScrollController();
  @override
  void initState() {
    super.initState();
    send();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);

    return Scaffold(
        body: NestedScrollView(
      controller: _controller,
      headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
        return <Widget>[
          _buildAppBar(),
        ];
      },
      body: ListView(
        padding: EdgeInsets.only(top: 10),
        children: [..._buildContentList()],
      ),
    ));
  }

  void send() async {
    print('发送网络请求，进行登录...');

    // var result = await LoginDao.login(userName!, password!);

    try {
      var result = await ProfileDao.get();
      print(result);
      setState(() {
        _profileMo = result;
      });
    } on NeedAuth catch (e) {
      print(e);
      showWarnToast(e.message);
    } on HiNetError catch (e) {
      print(e);
      showWarnToast(e.message);
    }
  }

  buildHead() {
    if (_profileMo == null)
      return Container(
          // child: Text('空空如也'),
          );
    return HiFlexibleHeader(
        name: _profileMo!.name,
        face: _profileMo!.face,
        controller: _controller);
  }

  // 页面保活
  @override
  bool get wantKeepAlive => true;

  _buildAppBar() {
    final screenSize = MediaQuery.of(context).size;

    return SliverAppBar(
      expandedHeight: 160, //扩展高度
      pinned: true, //标题栏是否固定
      flexibleSpace: FlexibleSpaceBar(
        titlePadding: EdgeInsets.only(left: 0),
        collapseMode: CollapseMode.parallax, //时间时差滚动效果
        title: buildHead(),
        background: Stack(
          children: [
            Positioned(
                child: cachedImage(
                    "https://img1.baidu.com/it/u=1320859100,2406848015&fm=253&fmt=auto&app=138&f=JPEG?w=750&h=500",
                    width: screenSize.width,
                    height: screenSize.height)),
            Positioned.fill(
                child: HiBlur(
              sigma: 2,
            )),
            Positioned(bottom: 0, left: 0, right: 0, child: _buildProfileTab())
          ],
        ),
      ),
    );
  }

  _buildContentList() {
    if (_profileMo == null) {
      return [];
    }
    return [
      _buildBanner(),
      CourseCard(
        courseList: _profileMo!.courseList,
      ),
      BenefitCard(
        benefitList: _profileMo!.benefitList,
      )
    ];
  }

  _buildBanner() {
    return HiBanner(_profileMo!.bannerList,
        bannerHeight: 120, padding: EdgeInsets.only(left: 10, right: 10));
  }

  _buildProfileTab() {
    if (_profileMo == null) {
      return Container();
    }
    return Container(
      padding: EdgeInsets.only(top: 5, bottom: 5),
      decoration: BoxDecoration(color: Colors.white54),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          _buildIconText('收藏', _profileMo!.favorite),
          _buildIconText('点赞', _profileMo!.like),
          _buildIconText('浏览', _profileMo!.browsing),
          _buildIconText('金币', _profileMo!.coin),
          _buildIconText('粉丝', _profileMo!.fans),
        ],
      ),
    );
  }

  _buildIconText(String text, int count) {
    return Column(
      //列
      children: [
        Text(
          '$count',
          style: TextStyle(fontSize: 15, color: Colors.black87),
        ),
        Text(
          '$text',
          style: TextStyle(fontSize: 12, color: Colors.grey[600]),
        )
      ],
    );
  }
}
