import 'package:flutter/material.dart';
import 'package:flutter_nested/flutter_nested.dart';
import 'package:smart/model/home_mo.dart';
import 'package:smart/widget/hi_banner.dart';

import '../core/hi_base_tab_state.dart';
import '../http/dao/home_dao.dart';
import '../model/video_model.dart';
import '../widget/video_card.dart';

class HomeTabPage extends StatefulWidget {
  final String? categoryName;
  final List<BannerMo>? bannerList;

  const HomeTabPage({Key? key, this.categoryName, this.bannerList});

  @override
  State<HomeTabPage> createState() => _HomeTabPageState();
}

class _HomeTabPageState
    extends HiBaseTabState<HomeMo, VideoModel, HomeTabPage> {
  @override
  void initState() {
    super.initState();
    print('initState  categoryName:${widget.categoryName}  ');
  }

  _banner() {
    return HiBanner(widget.bannerList!,
        padding: EdgeInsets.only(left: 5, right: 5));
  }

  @override
  bool get wantKeepAlive => true;

  @override
  // TODO: implement contentChild
  get contentChild => HiNestedScrollView(
      controller: scrollController,
      itemCount: dataList.length,
      // physics:const AlwaysScrollableScrollPhysics(),  在其它列表组件中 需要加一个属性。 flutter 默认，列表没有充满屏幕时候没有偶下拉加载和上划加载更多的功能的
      padding: EdgeInsets.only(top: 10, left: 10, right: 10),
      headers: [
        if (widget.bannerList != null)
          Padding(padding: EdgeInsets.only(bottom: 10), child: _banner())
      ],
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2, childAspectRatio: 0.82),
      itemBuilder: (BuildContext context, int index) {
        return VideoCard(videoMo: dataList[index]);
      });

  @override
  Future<HomeMo> getData(int pageIndex) async {
    HomeMo result = await HomeDao.get(widget.categoryName ?? '',
        pageIndex: pageIndex, pageSize: 10);
    return result;
  }

  @override
  List<VideoModel> parseList(HomeMo result) {
    return result.videoList;
  }
}
