import 'package:flutter/material.dart' hide NavigationBar;
import 'package:hi_base/view_util.dart';
import 'package:smart/widget/hi_tab.dart';
import 'package:smart/widget/navigation_bar.dart';

import 'ranking_tab_page.dart';

class RankingPage extends StatefulWidget {
  const RankingPage({super.key});

  @override
  State<RankingPage> createState() => _RankingPageState();
}

class _RankingPageState extends State<RankingPage>
    with TickerProviderStateMixin {
  static const TABS = [
    {'key': 'like', 'name': '最热'},
    {'key': 'pubdate', 'name': '最新'},
    {'key': 'favorite', 'name': '收藏'}
  ];
  late TabController _controller;

  @override
  void initState() {
    super.initState();
    _controller = TabController(length: TABS.length, vsync: this);
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
      children: [_buildNavigationBar(), _buildTabView()],
    ));
  }

  @override
  bool get wantKeepAlive => true;

  _buildNavigationBar() {
    return NavigationBar(
      child: Container(
        alignment: Alignment.center,
        child: _tabBar(),
        decoration: bottomBoxShadow(),
      ),
    );
  }

  _tabBar() {
    return HiTab(
      TABS.map((tab) {
        return Tab(
          text: tab['name'],
        );
      }).toList(),
      fontSize: 16,
      boderWidth: 3,
      unselectedLabelColor: Colors.black,
      controller: _controller,
    );
  }

  _buildTabView() {
    return Flexible(
        child: TabBarView(
      controller: _controller,
      children: TABS.map((tab) {
        return RankingTabPage(sort: tab['key']!);
      }).toList(),
    ));
  }
}
