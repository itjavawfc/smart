import 'package:flutter/material.dart';
import 'package:smart/core/hi_base_tab_state.dart';
import 'package:smart/http/dao/ranking_dao.dart';
import 'package:smart/model/ranking_mo.dart';
import 'package:smart/model/video_model.dart';
import 'package:smart/widget/video_large_card.dart';

class RankingTabPage extends StatefulWidget {
  final String sort;
  const RankingTabPage({super.key, required this.sort});

  @override
  State<RankingTabPage> createState() => _RankingTabPageState();
}

//<M, L, T extends StatefulWidget
class _RankingTabPageState
    extends HiBaseTabState<RankingMo, VideoModel, RankingTabPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  get contentChild => Container(
        child: ListView.builder(
            physics: AlwaysScrollableScrollPhysics(), // 当列表 没有填充屏幕的时候，允许可以下拉刷新
            padding: EdgeInsets.only(top: 10),
            itemCount: dataList.length,
            controller: scrollController,
            itemBuilder: (BuildContext context, int index) =>
                VideoLargeCard(videoModel: dataList[index])),
      );

  @override
  Future<RankingMo> getData(int pageIndex) async {
    RankingMo result =
        //  await RankingDao.get(widget.sort, pageIndex: pageIndex, pageSize: 1);
        await RankingDao.get(widget.sort, pageIndex: pageIndex, pageSize: 10);
    return result;
  }

  @override
  List<VideoModel> parseList(RankingMo result) {
    return result.list;
  }
}
