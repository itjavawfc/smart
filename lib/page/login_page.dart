import 'package:flutter/material.dart';
import 'package:hi_base/string_util.dart';
import 'package:hi_net/core/hi_error.dart';
import 'package:smart/http/dao/login_dao.dart';
import 'package:smart/navigator/hi_navigator.dart';
import 'package:smart/util/toast.dart';
import 'package:smart/widget/LoginButton.dart';
import 'package:smart/widget/appbar.dart';
import 'package:smart/widget/login_effect.dart';
import 'package:smart/widget/login_input.dart';

//登录页面
class LoginPage extends StatefulWidget {
/*  final VoidCallback? onJumpRegistion; //跳转注册页的回调
  final VoidCallback? onSuccess; //登录成功的回调*/
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  bool protect = false;
  bool loginEnable = false; // 是否可登录
  String? userName;
  String? password;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        theme: ThemeData(
          colorScheme: ColorScheme.fromSeed(seedColor: Colors.white),
          useMaterial3: true,
        ),
        home: Scaffold(
            appBar: appBar(
                '密码登录',
                '注册',
                () => {
                      HiNavigator.getInstance()
                          .onJumpTo(RouteStatus.registration)
                    }),

            //  appBar: appBar('注册', '登录', () {}),
            body: Container(
              child: ListView(children: [
                LoginEffect(protect: protect),
                LoginInput(
                  '用户名',
                  '请输入用户名',
                  onChanged: (text) {
                    //print(text);
                    userName = text;
                    checkInput();
                  },
                ),
                LoginInput(
                  '密码',
                  '请输入密码',
                  obscureText: true,
                  lineStretch: true,
                  onChanged: (text) {
                    //print(text);
                    password = text;
                    checkInput();
                  },
                  focusChanged: (focus) {
                    this.setState(() {
                      print('focus:$focus');
                      protect = focus;
                    });
                  },
                ),
                Padding(
                  padding: EdgeInsets.only(left: 20, right: 20, top: 20),
                  child: LoginButton(
                    '登录',
                    enable: loginEnable,
                    onPressed: send,
                  ),
                )
              ]),
            )));
  }

  void checkInput() {
    bool enable;
    if (isNotEmpty(userName!) && isNotEmpty(password!)) {
      enable = true;
    } else {
      enable = false;
    }
    setState(() {
      loginEnable = enable;
    });
  }

  _loginButton() {
    return InkWell(
      onTap: () {
        if (loginEnable) {
          checkParams();
        } else {
          print('loginEnable is false');
        }
      },
      child: Text('登录'),
    );
  }

  void send() async {
    print('发送网络请求，进行登录...');

    // var result = await LoginDao.login(userName!, password!);

    try {
      var result = await LoginDao.login(userName!, password!);
      print(result);
      if (result['code'] == 0) {
        print('登录成功  跳转到首页');
        showToast('登录成功');
        HiNavigator.getInstance().onJumpTo(RouteStatus.home);
      } else {
        print(result['msg']);
        showWarnToast(result['msg']);
      }
    } on NeedAuth catch (e) {
      print(e);
      showWarnToast(e.message);
    } on HiNetError catch (e) {
      print(e);
      showWarnToast(e.message);
    }
  }

  void checkParams() {
    String? tips;
    if (tips != null) {
      print(tips);
      return;
    }
    send();
  }
}
