
import 'package:flutter/material.dart';

class Box extends StatelessWidget {

  final String index;
  final double boxSize=100;

  const Box({super.key, required this.index});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: boxSize,
      height: boxSize,
      alignment: Alignment.center,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: [Colors.orangeAccent,Colors.orange,Colors.deepOrange]
        )
      ),
      child: Text(
        index,
        style: TextStyle(
          color: Colors.white,
          fontSize: 20,
          fontWeight: FontWeight.bold
        ),
      ),

    );
  }
}
