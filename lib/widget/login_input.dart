import 'package:flutter/material.dart';
import 'package:hi_base/color.dart';

class LoginInput extends StatefulWidget {
  //适配指南：可空的属性：通过 ? 进行修饰
  // 不可空的属性：在构造函数中设置默认值或者通过 required 进行修饰
  final String tile;
  final String hint;
  final ValueChanged<String>? onChanged; //监测用户框的输入
  final ValueChanged<bool>? focusChanged; //监测用户框获取到了焦点
  final bool lineStretch; //底部线是否撑满
  final bool obscureText; // 是否是密码
  final TextInputType? keyboardType; //定义输入框类型   键盘输入类型【文本、数字..】

  const LoginInput(this.tile, this.hint,
      {super.key, //key 表示 widget 的标识符. flutter 会为每一个 widget 自动指定 key 值
      this.onChanged,
      this.focusChanged,
      this.lineStretch = false,
      this.obscureText = false,
      this.keyboardType});

  @override
  State<LoginInput> createState() => _LoginInputState();
}

class _LoginInputState extends State<LoginInput> {
  final _focusNode = FocusNode();

  @override
  void initState() {
    super.initState();
    //  进行初始化操作
    // 是否获取光标的监听
    _focusNode.addListener(() {
      print('Has focus:${_focusNode.hasFocus}');
      if (widget.focusChanged != null) {
        widget.focusChanged!(_focusNode.hasFocus);
      }
    });
  }

  @override
  void dispose() {
    _focusNode.dispose(); //页面销毁的时候，释放focusNode
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            Container(
              padding: EdgeInsets.only(left: 15),
              width: 100,
              child: Text(widget.tile,
                  style: TextStyle(fontSize: 16),
                  textDirection: TextDirection.ltr),
            ),
            _input()
          ],
        ),
        Padding(
          padding: EdgeInsets.only(left: !widget.lineStretch ? 15 : 0),
          child: Divider(
            height: 1,
            thickness: 0.5, //线的粗细
          ),
        )
      ],
    );
  }

  _input() {
    return Expanded(
        //用于展开，填充所有空间
        child: TextField(
      focusNode: _focusNode,
      onChanged: widget.onChanged,
      obscureText: widget.obscureText,
      keyboardType: widget.keyboardType,
      autofocus: !widget.obscureText,
      cursorColor: primary,
      textDirection: TextDirection.ltr,
      style: TextStyle(
          fontSize: 16, color: Colors.black, fontWeight: FontWeight.w300),
      // 输入框样式
      decoration: InputDecoration(
          contentPadding: EdgeInsets.only(left: 20, right: 20),
          border: InputBorder.none,
          hintText: widget.hint ?? '',
          hintStyle: TextStyle(fontSize: 15, color: Colors.blue[100])),
    ));
  }
}
