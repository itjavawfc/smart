import 'package:flutter/material.dart';
import 'package:hi_base/format_util.dart';
import 'package:smart/model/video_model.dart';

// 详情页  作者 widget
class VideoHeader extends StatelessWidget {
  final Owner owner;

  const VideoHeader({super.key, required this.owner});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 15, left: 15, right: 15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(15),
                child: Image.network(owner.face, width: 30, height: 30),
              ),
              Padding(
                padding: EdgeInsets.only(left: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      owner.name,
                      style: TextStyle(
                          fontSize: 13,
                          color: Colors.grey,
                          fontWeight: FontWeight.bold),
                    ),
                    Text(
                      '${countFormat(owner.fans)}粉丝',
                      style: TextStyle(fontSize: 10, color: Colors.grey[400]),
                    )
                  ],
                ),
              )
            ],
          ),
          MaterialButton(
            onPressed: () {
              print('---- 关注------');
            },
            color: Colors.pink[300],
            height: 24,
            minWidth: 50,
            child: Text('+关注',
                style: TextStyle(color: Colors.white, fontSize: 13)),
          )
        ],
      ),
    );
  }
}
