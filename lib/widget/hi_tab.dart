/*
TabBar(
        controller: _controller,
        isScrollable: true, //是否可以滑动
        labelColor: Colors.black,
        indicator: UnderlineIndicator(
          strokeCap: StrokeCap.round,
          borderSide: BorderSide(color: primary, width: 3),
          insets: EdgeInsets.only(left: 15, right: 15),
        ),
        tabs: categoryList.map<Tab>((tab) {
          return Tab(
            child: Padding(
              padding: EdgeInsets.only(left: 5, right: 5, top: 5, bottom: 5),
              child: Text(
                tab.name!!,
                style: TextStyle(fontSize: 16),
              ),
            ),
          );
        }).toList());
 */

import 'package:flutter/material.dart';
import 'package:underline_indicator/underline_indicator.dart';

class HiTab extends StatelessWidget {
  final List<Widget> tabs;
  final TabController? controller;
  final double? fontSize; // size 大小
  final double? boderWidth; //边距大小
  final double? insets; // 边距距离
  final Color? unselectedLabelColor;

  const HiTab(this.tabs,
      {super.key,
      this.controller,
      this.fontSize = 13,
      this.boderWidth = 2,
      this.insets = 15,
      this.unselectedLabelColor = Colors.grey});

  @override
  Widget build(BuildContext context) {
    return TabBar(
        controller: controller,
        isScrollable: true, //是否可以滑动
        labelColor: Colors.pink,
        unselectedLabelColor: unselectedLabelColor,
        labelStyle: TextStyle(fontSize: fontSize),
        indicator: UnderlineIndicator(
          strokeCap: StrokeCap.round, //设置为圆角或者方角
          borderSide: BorderSide(color: Colors.pink, width: boderWidth!!),
          insets: EdgeInsets.only(left: insets!!, right: insets!!),
        ),
        tabs: tabs);
  }
}
