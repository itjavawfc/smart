//增值服务
import 'package:flutter/material.dart';
import 'package:hi_base/view_util.dart';
import 'package:smart/model/profile_mo.dart';
import 'package:smart/widget/hi_blur.dart';

//class BenefitCard extends StatelessWidget {
class BenefitCard extends StatelessWidget {
  final List<Benefit> benefitList;

  const BenefitCard({super.key, required this.benefitList});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 10, right: 5, top: 15),
      child: Column(
        children: [_buildTitle(), _buildBenefit(context)],
      ),
    );
  }

  _buildTitle() {
    return Container(
      padding: EdgeInsets.only(bottom: 10),
      child: Row(
        children: [
          Text('增值服务',
              style: TextStyle(
                  fontSize: 18,
                  //color: Colors.black54,
                  fontWeight: FontWeight.bold)),
          hiSpace(width: 10),
          Text('进一步学习，进阶',
              style: TextStyle(fontSize: 12, color: Colors.grey[600]))
        ],
      ),
    );
  }

  // 动态布局
  _buildBenefit(BuildContext context) {
    //根据卡片数量计算出每一个卡片的宽度
    var width = (MediaQuery.of(context).size.width -
            20 -
            (benefitList.length - 1) * 5) / // 20是左右边距  5是每个卡片间距
        benefitList.length;
    // var height = width / 16 * 9;
    return Row(
      children: [
        ...benefitList.map((mo) => _buildCard(context, mo, width)).toList()
      ],
    );
  }

  _buildCard(BuildContext context, Benefit mo, double width) {
    return InkWell(
      onTap: () {
        print('点击了...');
      },
      child: Padding(
        padding: EdgeInsets.only(right: 5, bottom: 7),
        child: ClipRRect(
            borderRadius: BorderRadius.circular(3),
            child: Container(
              alignment: Alignment.center,
              width: width,
              height: 60,
              decoration: BoxDecoration(color: Colors.deepOrangeAccent),
              child: Stack(
                children: [
                  Positioned.fill(child: HiBlur()),
                  Positioned.fill(
                      child: Center(
                    child: Text(
                      mo.name,
                      style: TextStyle(
                        color: Colors.white54,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ))
                ],
              ),
            )),
      ),
    );
  }
}
