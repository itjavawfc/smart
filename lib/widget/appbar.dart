//自定义顶部得appBar
import 'package:flutter/material.dart';
import 'package:hi_base/view_util.dart';

appBar(String title, String rightTitle, VoidCallback rightButtonClick) {
  return AppBar(
    //让title 居左
    centerTitle: false, //是否居中
    titleSpacing: 0,
    leading: BackButton(), //左侧的action 功能,返回按键：也可以自己定义一个返回按键
    title: Text(
      title,
      style: TextStyle(fontSize: 18),
    ),
    actions: [
      //右侧的action 功能
      InkWell(
        //具备各种事件，如果需要相应，可以外部包裹一层
        onTap: rightButtonClick,
        child: Container(
          padding: EdgeInsets.only(left: 15, right: 15),
          alignment: Alignment.center,
          child: Text(
            rightTitle,
            style: TextStyle(fontSize: 18, color: Colors.grey[500]),
            textAlign: TextAlign.center,
          ),
        ),
      )
    ],
  );
}

// 视频详情页的appBar
videoAppBar() {
  return Container(
    padding: EdgeInsets.only(right: 8),
    decoration: BoxDecoration(gradient: blackLinearGradient(fromTop: true)),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        BackButton(color: Colors.white),
        Row(
          children: [
            Icon(Icons.live_tv_rounded, color: Colors.white, size: 20),
            Padding(
              padding: EdgeInsets.only(left: 12),
              child: Icon(
                Icons.more_vert_rounded,
                color: Colors.white,
                size: 20,
              ),
            )
          ],
        )
      ],
    ),
  );
}
