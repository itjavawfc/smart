import 'package:flutter/material.dart';
import 'package:hi_base/view_util.dart';

//定义一个枚举

// enum StatusStyle { DEFAULT, LIGHT_CONTENT, DARK_CONTENT }

// 可自定义样式的沉浸式导航栏
class NavigationBar extends StatefulWidget {
  /* final StatusStyle statusStyle;
  final Color color;
  final double height;
  final Widget? child;

  const NavigationBar(
      {super.key,
      this.statusStyle = StatusStyle.DARK_CONTENT,
      required this.color,
      required this.height,
      this.child});*/

  final StatusStyle statusStyle;
  final Color color;
  final double height;
  final Widget? child;

  const NavigationBar(
      {Key? key,
      this.statusStyle = StatusStyle.DARK_CONTENT,
      this.color = Colors.white,
      this.height = 46,
      this.child})
      : super(key: key);

  @override
  State<NavigationBar> createState() => _NavigationBar();
}

class _NavigationBar extends State<NavigationBar> {
  @override
  void initState() {
    //只一次调用
    super.initState();
    _statusBarInit();
  }

  @override
  Widget build(BuildContext context) {
    //状态栏的高度
    var top = MediaQuery.of(context).padding.top;
    return Container(
      width: MediaQuery.of(context).size.width,
      // height: top + height,
      height: top + widget.height,
      child: widget.child,
      padding: EdgeInsets.only(top: top),
      decoration: BoxDecoration(color: widget.color),
    );
  }

  void _statusBarInit() {
    //沉浸式状态栏
    changeStatusBar(color: widget.color, statusStyle: widget.statusStyle);
  }
}
/* final StatusStyle statusStyle;
  final Color color;
  final double height;
  final Widget child;

  const HiNavigationBar(
      {super.key,
      this.statusStyle = StatusStyle.DARK_CONTENT,
      required this.color,
      required this.height,
      required this.child});

  @override
  Widget build(BuildContext context) {
    _statusBarInit();
    //状态栏的高度
    var top = MediaQuery.of(context).padding.top;
    return Container(
      width: MediaQuery.of(context).size.width,
      // height: top + height,
      height: top + height,
      child: child,
      padding: EdgeInsets.only(top: top),
      decoration: BoxDecoration(color: color),
    );
  }

  void _statusBarInit() {
    //沉浸式状态栏
    changeStatusBar(color: color, statusStyle: statusStyle);
  }
}
*/
