import 'package:flutter/material.dart';
import 'package:hi_base/format_util.dart';
import 'package:hi_base/view_util.dart';
import 'package:smart/model/video_model.dart';
import 'package:smart/navigator/hi_navigator.dart';

class VideoLargeCard extends StatelessWidget {
  final VideoModel videoModel;

  const VideoLargeCard({super.key, required this.videoModel});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      //GestureDetector 可定义的手势比较多
      onTap: () {
        HiNavigator.getInstance()
            .onJumpTo(RouteStatus.detail, args: {'videoMo': videoModel});
      },
      child: Container(
        margin: EdgeInsets.only(left: 15, right: 15, bottom: 5),
        padding: EdgeInsets.only(bottom: 6),
        // height: 106,
        // decoration: BoxDecoration(border: borderLine(context)),
        child: Row(
          children: [_itemImage(context), _buildContent()],
        ),
      ),
    );
  }

  _itemImage(BuildContext context) {
    double height = 90;
    return ClipRRect(
      borderRadius: BorderRadius.circular(5),
      child: Stack(
        //String url, {double? width, double? height
        children: [
          cachedImage(videoModel.cover,
              width: height * (16 / 9), height: height),
          Positioned(
              bottom: 5,
              right: 5,
              child: Container(
                padding: EdgeInsets.all(2),
                decoration: BoxDecoration(
                    // 显示时间的Text 加一个容器的背景，装饰器：borderRadius  让Text 一定看得见。
                    color: Colors.grey,
                    borderRadius: BorderRadius.circular(1)),
                child: Text(
                  durationTransform(videoModel.duration),
                  style: TextStyle(color: Colors.white, fontSize: 8),
                ),
              ))
        ],
      ),
    );
  }

  _buildContent() {
    return Expanded(
      child: Container(
          padding: EdgeInsets.only(top: 5, left: 8, right: 8, bottom: 5),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(videoModel.title,
                  style: TextStyle(fontSize: 12, color: Colors.black)),
              _buildBottomContent()
            ],
          )),
    );
  }

  _buildBottomContent() {
    return Column(
      children: [
        //作业
        _owner(),
        hiSpace(height: 5),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                ...smallIconText(Icons.ondemand_video, videoModel.view),
                Padding(padding: EdgeInsets.only(left: 5)),
                ...smallIconText(Icons.list_alt, videoModel.reply),
                Padding(padding: EdgeInsets.only(left: 5)),
                //Text('$dateStr', style: style)
              ],
            ),
            Icon(
              Icons.more_vert_sharp,
              color: Colors.grey,
              size: 14,
            )
          ],
        )
      ],
    );
  }

  _owner() {
    var owner = videoModel.owner;
    return Row(
      children: [
        Container(
            padding: EdgeInsets.all(2),
            decoration: BoxDecoration(
                //  color: Colors.grey,
                borderRadius: BorderRadius.circular(2),
                border: Border.all(color: Colors.grey)), // 外面套一个圈
            child: Text(
              'Up',
              style: TextStyle(fontSize: 11, color: Colors.grey),
            )),
        hiSpace(width: 18),
        Text(
          owner.name,
          style: TextStyle(fontSize: 11, color: Colors.grey),
        ),
      ],
    );
  }
}
