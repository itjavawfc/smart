import 'package:flutter/material.dart';
import 'package:hi_base/view_util.dart';
import 'package:smart/model/profile_mo.dart';

//职场进阶
class CourseCard extends StatelessWidget {
  final List<Course> courseList;

  const CourseCard({super.key, required this.courseList});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 10, right: 5, top: 15),
      child: Column(
        children: [_buildTitle(), ..._buildCardList(context)],
      ),
    );
  }

  _buildTitle() {
    return Container(
      padding: EdgeInsets.only(bottom: 10),
      child: Row(
        children: [
          Text('进阶',
              style: TextStyle(
                  fontSize: 18,
                  //color: Colors.black54,
                  fontWeight: FontWeight.bold)),
          hiSpace(width: 10),
          Text('带你突破技术瓶颈',
              style: TextStyle(fontSize: 12, color: Colors.grey[600]))
        ],
      ),
    );
  }

  // 动态布局
  _buildCardList(BuildContext context) {
    var courseGroup = Map();
    // 将课程数据进行分组  //每一组就是显示每一行
    courseList.forEach((mo) {
      if (!courseGroup.containsKey(mo.group)) {
        courseGroup[mo.group] = []; //创建一个group  map的value 是一个数组
      }
      ////这个地方很经典： courseGroup[mo.group] 对应的是Map 通过key 来获取value 的值，是个数组。给数组加一个值，引用传递
      // 这样courseGroup 就有值了 key,value ==> 课程名字：课程里面的 mo
      List list = courseGroup[mo.group];
      list.add(mo);
    });

    // 在通过map 遍历，动态创建Row
    return courseGroup.entries.map((e) {
      List list = e.value;
      //根据卡片数量计算出每一个卡片的宽度
      var width = (MediaQuery.of(context).size.width -
              20 -
              (list.length - 1) * 5) / // 20是左右边距  5是每个卡片间距
          list.length;
      var height = width / 16 * 9;
      return Row(
        children: [...list.map((mo) => buildCard(mo, width, height)).toList()],
      );
    });
  }

  buildCard(Course mo, double width, double height) {
    return InkWell(
      onTap: () {
        print('点击了...');
      },
      child: Padding(
        padding: EdgeInsets.only(right: 5, bottom: 7),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(3),
          child: cachedImage(mo.cover, width: width, height: height),
        ),
      ),
    );
  }
}
