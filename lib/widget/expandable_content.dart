// 可展开的widget
import 'package:flutter/material.dart';
import 'package:hi_base/view_util.dart';
import 'package:smart/model/video_model.dart';

class ExpandableContent extends StatefulWidget {
  final VideoModel model;

  const ExpandableContent({super.key, required this.model});

  @override
  State<ExpandableContent> createState() => _ExpandableContentState();
}

class _ExpandableContentState extends State<ExpandableContent>
    with SingleTickerProviderStateMixin {
  static final Animatable<double> _easeInTween =
      CurveTween(curve: Curves.easeIn); //声明一个补间动画  curve 动画展现的效果，可以自己选择设定
  bool _expand = false; // 默认不展开

  // 用来管理 Animation
  AnimationController? _controller;
  //生成动画高度的值
  Animation<double>? _heightFactor;

  @override
  void initState() {
    super.initState();
    //初始化东海
    _controller =
        AnimationController(duration: Duration(milliseconds: 200), vsync: this);
    _heightFactor = _controller?.drive(_easeInTween); //生成动画
    _controller?.addListener(() {
      //监听动画值的变化
      print(_heightFactor!.value);
    });
  }

  @override
  void dispose() {
    super.dispose();
    _controller?.dispose(); //动画销毁
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 15, right: 15, top: 5),
      child: Column(children: [
        _buildTitle(),
        Padding(padding: EdgeInsets.only(bottom: 8)),
        _buildInfo(),
        _buildDes()
      ]),
    );
  }

  _buildTitle() {
    return InkWell(
      onTap: _toggleExpand,
      child: Row(
        //行
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          // 通过Expanded  让Text 获取最大宽度，以便显示省略号
          Expanded(
            child: Text(
              widget.model.title,
              maxLines: 1,
              overflow: TextOverflow.ellipsis, //多余的显示出来省略号
            ),
          ),
          Padding(padding: EdgeInsets.only(left: 15)),
          Icon(
            _expand
                ? Icons.keyboard_arrow_up_sharp
                : Icons.keyboard_arrow_down_sharp,
            color: Colors.grey,
            size: 16,
          )
        ],
      ),
    );
  }

  void _toggleExpand() {
    setState(() {
      _expand = !_expand; // 定义的boolean 变量取反
      if (_expand) {
        //执行动画
        _controller?.forward();
      } else {
        //反向执行动画
        _controller?.reverse();
      }
    });
  }

  _buildInfo() {
    var style = TextStyle(fontSize: 12, color: Colors.grey);

    var dateStr = widget.model.createTime.length > 10
        ? widget.model.createTime.substring(5, 10)
        : widget.model.createTime;

    return Row(
      // mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        ...smallIconText(Icons.ondemand_video, widget.model.view),
        Padding(padding: EdgeInsets.only(left: 15)),
        ...smallIconText(Icons.list_alt, widget.model.reply),
        Padding(padding: EdgeInsets.only(left: 15)),
        Text('$dateStr', style: style)
      ],
    );
  }

  _buildDes() {
    var child = _expand
        ? Text(widget.model.desc,
            style: TextStyle(fontSize: 12, color: Colors.grey))
        : null;
    //构建动画的通用widget
    return AnimatedBuilder(
        animation: _controller!.view,
        child: child,
        builder: (BuildContext context, Widget? child) {
          return Align(
            heightFactor: _heightFactor?.value,
            // fix 从布局之上的位置开始展开
            alignment: Alignment.topCenter,
            child: Container(
              // 会撑满宽度后，让内容对其
              alignment: Alignment.topLeft,
              padding: EdgeInsets.only(top: 8),
              child: child,
            ),
          );
        });
  }
}
