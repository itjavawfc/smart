// 带Lottie 动画加载进度条组件
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

class LoadingContainer extends StatelessWidget {
  final Widget child; //在哪一个组件之上
  final bool isLoading;
  // 加载东海 是否覆盖在原有界面上
  final bool conver;

  const LoadingContainer(
      {super.key,
      required this.child,
      required this.isLoading,
      this.conver = false});

  Widget get _loadingView {
    return Center(child: Lottie.asset('assets/loading.json'));
  }

  @override
  Widget build(BuildContext context) {
    if (conver) {
      return Stack(
        children: [child, isLoading ? _loadingView : Container()],
      );
    } else {
      return isLoading ? _loadingView : child;
    }
    return Container();
  }

  // lottie 动画
}
