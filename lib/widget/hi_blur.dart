//高斯模糊

import 'dart:ui';

import 'package:flutter/material.dart';

class HiBlur extends StatelessWidget {
  final Widget? child;

  final double sigma; // 模糊值

  const HiBlur({super.key, this.child, this.sigma = 10});

  @override
  Widget build(BuildContext context) {
    return BackdropFilter(
      filter: ImageFilter.blur(sigmaX: sigma, sigmaY: sigma),
      child: Container(
        color: Colors.white10,
        child: child,
      ),
    );
  }
}
